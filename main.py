# -import libs
import traceback

# -define net
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from tqdm.autonotebook import tqdm

from dataloader import *


# define net
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(32, 64, 5)
        self.fc1 = nn.Linear(64*53*53, 1024)
        self.fc2 = nn.Linear(1024, 7)
        #self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        # print(x.shape)
        x = x.view(x.size(0), -1)
        x = F.relu(self.fc1(x))
        #x = F.relu(self.fc2(x))
        x = self.fc2(x)
        return x

net = Net()

root_dir = r"D:\kimnh3\synth_text\synth_output_1mil_en"
set = "train"

size_to_resize = 224

data = LoadData(root_dir, set, transform=transforms.Compose([
    Resizer(size_to_resize), Normalizer(),
    ToTensor()
])
                )
training_generator = DataLoader(data, batch_size=4,
                                shuffle=True, num_workers=0)

num_iter_per_epoch = len(training_generator)

step = 0

# lost func and optim
import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
for epoch in range(10):
    last_epoch = step // num_iter_per_epoch
    if epoch < last_epoch:
        continue

    epoch_loss = []
    progress_bar = tqdm(training_generator)

    # lost value init
    running_loss = 0.0

    for i, data in enumerate(progress_bar):
        if i < step - last_epoch * num_iter_per_epoch:
            progress_bar.update()
            continue
        try:
            imgs = data['image']
            annot = data['annot']

            # zero the parameter gradients
            optimizer.zero_grad()
            y_tensor = torch.tensor(imgs, dtype=torch.long)
            # forward + backward + optimize
            outputs = net(y_tensor)
            loss = criterion(outputs, annot)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 2000 == 1999:  # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0

            # print(imgs.size)
            #
            # grid = utils.make_grid(imgs)
            # plt.imshow(imgs.transpose((1, 2, 0)))
            #
            # plt.show()
            # img = imgs[1].numpy().reshape(224,224,3)
            # plt.imshow("1",img)
            # cv2.imwrite("test.jpg",img)
            # print("annot: ",annot)
        except Exception as e:
            print('[Error]', traceback.format_exc())
            print(e)
            continue

    """Show sample image"""
    # for iter, data in enumerate(progress_bar):
    #     imgs = data['image']
    #     annot = data['annot']
    #     batch_size = len(imgs)
    #     im_size = imgs.size(2)
    #     grid_border_size = 2
    #     cv2.imwrite("test.jpg",img)
    #     break
    # break

print("Finished training")
PATH = './text_rotation_model.pth'
torch.save(net.state_dict(), PATH)
