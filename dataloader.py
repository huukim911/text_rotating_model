import os
import random

import torch
import numpy as np
import cv2
from torch.utils.data import Dataset
from skimage import io, transform
import matplotlib.pyplot as plt


class LoadData(Dataset):
    def __init__(self, root_dir, set='train', transform=None):
        self.root_dir = root_dir
        self.set_name = set
        self.transform = transform
        self.image_ids = self.load_image_id()
        # self.load_classes()

    # get length of list of image
    def __len__(self):
        return len(self.image_ids)

    def load_image_id(self):
        path_train = os.path.join(self.root_dir, 'train')
        list_data = os.listdir(path_train)
        for i in range(len(list_data)):
            list_data[i] = os.path.splitext(list_data[i])[0]
        return list_data

    def load_image(self, image_idx):
        image_file = os.path.join(self.root_dir, 'train', self.image_ids[image_idx] + '.jpg')
        img = cv2.imread(image_file)
        image_tmp = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        return image_tmp.astype(np.float32) / 255.

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_name = os.path.join(self.root_dir, 'train', self.image_ids[idx] + '.jpg')
        image = io.imread(img_name)
        print(type(image))
        print(img_name)
        rand_rect = random.randint(0, 360)

        #90 clockwise
        if 0 < rand_rect <=90:
            annot = 1
            image = cv2.rotate(image, cv2.ROTATE_90_CLOCKWISE)
        #180 clockwise
        elif 90 < rand_rect <=180:
            annot = 2
            image = cv2.rotate(image, cv2.ROTATE_180)
        elif 181 < rand_rect <=270:
            annot = 3
            image = cv2.rotate(image, cv2.ROTATE_90_COUNTERCLOCKWISE)
        else:
            annot = 0
        annot = np.array(annot)
        sample = {'image': image, 'annot': annot}

        if self.transform:
            sample = self.transform(sample)

        return sample

class Resizer(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, img_size=224):
        self.img_size = img_size

    def __call__(self, sample):
        image, annot = sample['image'], sample['annot']
        height, width, _ = image.shape

        if height > width:
            scale = self.img_size / height
            resized_height = self.img_size
            resized_width = int(width * scale)
        else:
            scale = self.img_size / width
            resized_height = int(height * scale)
            resized_width = self.img_size
        # print(resized_height, resized_width)
        image = cv2.resize(image, (resized_width, resized_height), interpolation=cv2.INTER_LINEAR)
        # cv2.imshow("Resized image", image)
        # plt.imshow("1",image)

        # print(self.img_size)
        delta_w = self.img_size - resized_width
        delta_h = self.img_size - resized_height
        top, bottom = delta_h // 2, delta_h - (delta_h // 2)
        left, right = delta_w // 2, delta_w - (delta_w // 2)

        color = [255, 255, 255]
        new_im = cv2.copyMakeBorder(image, top, bottom, left, right, cv2.BORDER_CONSTANT,
                                    value=color)
        # new_image = np.zeros((self.img_size, self.img_size, 3))
        # new_image[0:resized_height, 0:resized_width] = image
        cv2.imwrite("test0.jpg",new_im)
        return {'image': new_im, 'annot': annot}

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, annot = sample['image'], sample['annot']
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image.transpose((2, 0, 1))
        # print("annot 1: ", annot)
        return {'image': torch.from_numpy(image).to(torch.float32),
                'annot': torch.from_numpy(annot)}


class Normalizer(object):

    def __init__(self, mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]):
        self.mean = np.array([[mean]])
        self.std = np.array([[std]])

    def __call__(self, sample):
        image, annots = sample['image'], sample['annot']

        return {'image': ((image.astype(np.float32) - self.mean) / self.std), 'annot': annots}